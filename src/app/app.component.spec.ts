import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let appComponent = new AppComponent();

  it('should create the app', () => {
    const app = appComponent;
    expect(app).toBeTruthy();
  });

  it('should have `Welcome to Integrated Digital Platform!` name', () => {
    const name = appComponent.name;
    expect(name).toEqual('Welcome to Integrated Digital Platform');
  });
});