import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { HomechartComponent } from './homechart.component';
@NgModule({
  imports:      [ BrowserModule, FormsModule, HighchartsChartModule ],
  declarations: [ AppComponent, HelloComponent, HomechartComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
